package daemonrun

import (
	"errors"
	"flag"
	"fmt"
	"io/ioutil"
	"os"
	"os/exec"
	"os/signal"
	"strconv"
	"strings"
	"syscall"
)

type Runnable func(started, stopped chan<- bool)

var runSwitch = "--daemon"

var doStart = flag.Bool("start", false, "Start the application as a daemon")
var doStop = flag.Bool("stop", false, "Stop the application if running as a daemon")
var doDaemon = flag.Bool("daemon", false, "Run the application in the background (not normally used from command line).")
var doRun = flag.Bool("run", false, "Run the application in the foreground")

func ExecRun(run Runnable) {
	flag.Parse()

	var started, stopped chan bool

	if *doStart { // <- Start the app as daemon
		if err := startProcess(); err != nil {
			fmt.Println(err.Error())
		}

	} else if *doStop { // <- Stop the running daemon
		if err := stopProcess(); err != nil {
			fmt.Println(err.Error())
		}

	} else if *doDaemon { // <- Run the app in the background -- used by daemon mode
		if err := writePidFile(); err != nil {
			fmt.Println(err.Error())

		} else {
			started = make(chan bool)
			stopped = make(chan bool)
			go run(started, stopped) // <- Main entry point to the app
			<-started
			if err := signalStartDone(); err != nil {
				fmt.Println(err.Error())
			}
			<-stopped
			if err := removePidFile(); err != nil {
				fmt.Println(err.Error())
			}

			if err = signalStopDone(); err != nil {
				fmt.Println(err.Error())
			}
		}
	} else if *doRun { // <- Run the app in the foreground
		started = make(chan bool, 1)
		stopped = make(chan bool, 1)
		run(started, stopped) // <- Main entry point
	} else {
		flag.PrintDefaults()
	}

	return
}

func writePidFile() error {

	pidFilePath, err := getPidPath()
	if err != nil {
		return errors.New(fmt.Sprintf("Couldn't get PID file path: %s", err.Error()))
	}

	var myPid = os.Getpid()

	if _, err = os.Stat(pidFilePath); !os.IsNotExist(err) {
		if err != nil {
			return errors.New(fmt.Sprintf("Couldn't write PID file: %s", err.Error()))
		} else {
			return errors.New("Couldn't write PID file - already exists.")
		}
	}

	if err = ioutil.WriteFile(pidFilePath, []byte(strconv.FormatInt(int64(myPid), 10)), 0666); err != nil {
		return errors.New(fmt.Sprintf("Couldn't write PID file: %s", err.Error()))
	}

	return nil
}

func removePidFile() error {
	var pidFilePath, err = getPidPath()
	if err != nil {
		return errors.New(fmt.Sprintf("Couldn't get PID file path: %s", err.Error()))
	}

	if err = os.Remove(pidFilePath); err != nil {
		return errors.New(fmt.Sprintf("Couldn't remove PID file: %s", err.Error()))
	}

	return nil
}

func startProcess() error {
	var startDoneChan = make(chan os.Signal)
	signal.Notify(startDoneChan, syscall.SIGUSR1)

	var myProcPath, myExecName, err = getExecName()
	if err != nil {
		return errors.New(fmt.Sprintf("Could not get target path for executable: %s", err.Error()))
	}

	var myPid = os.Getpid()
	if err = ioutil.WriteFile("me.pid", []byte(strconv.FormatInt(int64(myPid), 10)), 0666); err != nil {
		return errors.New(fmt.Sprintf("Couldn't write me.pid: %s", err.Error()))
	}

	var myWorkingDirectory = strings.TrimRight(myProcPath, myExecName)

	var cmd = exec.Command(myProcPath, runSwitch)
	cmd.Dir = myWorkingDirectory
	cmd.Stdout = os.Stdout
	err = cmd.Start()
	if err != nil {
		return errors.New(fmt.Sprintf("Couldn't start application as daemon: %s", err.Error()))
	}

	<-startDoneChan

	if err = os.Remove("me.pid"); err != nil {
		return errors.New(fmt.Sprintf("Couldn't remove me.pid: %s", err.Error()))
	}

	return nil
}

func signalStartDone() error {
	pidBytes, err := ioutil.ReadFile("me.pid")
	if err != nil {
		return errors.New(fmt.Sprintf("Couldn't read target PID from PID file: %s", err.Error()))
	}

	var pid int64
	pid, err = strconv.ParseInt(string(pidBytes), 10, 32)
	if err != nil {
		return errors.New(fmt.Sprintf("Invalid PID value %s	 - %s", string(pidBytes), err.Error()))
	}

	err = syscall.Kill(int(pid), syscall.SIGUSR1)
	if err != nil {
		return errors.New(fmt.Sprintf("Couldn't started signal: %s", err.Error()))
	}

	return nil
}

func stopProcess() error {
	var stopDoneChan = make(chan os.Signal)
	signal.Notify(stopDoneChan, syscall.SIGUSR2)

	var myPid = os.Getpid()
	if err := ioutil.WriteFile("me.pid", []byte(strconv.FormatInt(int64(myPid), 10)), 0666); err != nil {
		return errors.New(fmt.Sprintf("Couldn't write me.pid: %s", err.Error()))
	}

	var pidFilePath, err = getPidPath()
	if err != nil {
		return errors.New(fmt.Sprintf("Couldn't get PID file path: %s", err.Error()))
	}

	var pidBytes []byte
	pidBytes, err = ioutil.ReadFile(pidFilePath)
	if err != nil {
		return errors.New(fmt.Sprintf("Couldn't read target PID from PID file: %s", err.Error()))
	}

	var pid int64
	pid, err = strconv.ParseInt(string(pidBytes), 10, 32)
	if err != nil {
		return errors.New(fmt.Sprintf("Invalid PID value %s	 - %s", string(pidBytes), err.Error()))
	}

	err = syscall.Kill(int(pid), syscall.SIGINT)
	if err != nil {
		return errors.New(fmt.Sprintf("Couldn't signal daemon to stop: %s", err.Error()))
	}

	<-stopDoneChan

	if err = os.Remove("me.pid"); err != nil {
		return errors.New(fmt.Sprintf("Couldn't remove me.pid: %s", err.Error()))
	}

	return nil

}

func signalStopDone() error {
	pidBytes, err := ioutil.ReadFile("me.pid")
	if err != nil {
		return errors.New(fmt.Sprintf("Couldn't read target PID from PID file: %s", err.Error()))
	}

	var pid int64
	pid, err = strconv.ParseInt(string(pidBytes), 10, 32)
	if err != nil {
		return errors.New(fmt.Sprintf("Invalid PID value %s	 - %s", string(pidBytes), err.Error()))
	}

	err = syscall.Kill(int(pid), syscall.SIGUSR2)
	if err != nil {
		return errors.New(fmt.Sprintf("Couldn't stopped signal: %s", err.Error()))
	}

	return nil
}

func getPidPath() (string, error) {

	var _, myExecName, err = getExecName()
	if err != nil {
		return "", errors.New(fmt.Sprintf("Couldn't get exec name: %s", err.Error()))
	}

	var myUid = os.Getuid()

	if myUid > 0 {
		if err := makeUserPidDirectory(myUid, myExecName); err != nil {
			return "", err
		}
		return fmt.Sprintf("/var/run/user/%d/%s/pid", myUid, myExecName), nil
	} else {
		return fmt.Sprintf("/var/run/%s.pid", myExecName), nil
	}
}

func makeUserPidDirectory(uid int, execName string) error {
	if err := os.MkdirAll(fmt.Sprintf("/var/run/user/%d/%s", uid, execName), 0755); err != nil && !os.IsExist(err) {
		return errors.New(fmt.Sprintf("Couldn't make PID file directory: %s", err.Error()))
	}

	return nil
}

func getExecName() (string, string, error) {
	var myProcPath, err = os.Executable()
	if err != nil {
		return "", "", err
	}

	var myProcPathComponents = strings.Split(myProcPath, "/")
	var myExecName = myProcPathComponents[len(myProcPathComponents)-1]

	return myProcPath, myExecName, nil
}
